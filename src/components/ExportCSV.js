import React, { Component } from 'react';
import {
  Search,
  Edit,
  Trash,
  Pause,
  Slash,
  FileText,
  Download,
  Settings,
  File,
  Check,
  CheckCircle
} from "react-feather";
import Modal from "./modal";
import moment from "moment";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

class ExportCSV extends Component{
  fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  fileExtension = '.xlsx';

    exportToCSV = (csvData, fileName) => {
        const ws = XLSX.utils.json_to_sheet(csvData);
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], {type: fileType});
        FileSaver.saveAs(data, fileName + fileExtension);
    }

    // return (
    //     // <Button variant="warning" onClick={(e) => exportToCSV(csvData,fileName)}>Export</Button>
    // )
}