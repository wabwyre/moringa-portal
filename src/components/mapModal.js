import React, { Component } from "react";
import Modal from "./modal";
import PlacesAutocomplete from "react-places-autocomplete";
import {
  geocodeByAddress,
  geocodeByPlaceId,
  getLatLng
} from "react-places-autocomplete";
import { MapPin } from "react-feather";
import GoogleMapReact from "google-map-react";
import { fitBounds } from "google-map-react/utils";
import marker from "../img/marker.png";
import Geocode from "react-geocode";

let key = "AIzaSyBrXomkQVD93H0PoUN-hkF3U04aVjk46NQ";
Geocode.setApiKey(key);
Geocode.enableDebug();

class MapModal extends Component {
  state = {
    address: "",
    center: { lat: -4.0434771, lng: 39.6682065 },
    zoom: 13,
    bounds: {},
    visible: this.props.visible
  };

  timeout = null;

  handleChange = address => {
    clearTimeout(this.timeout);
    // let $t = this;
    // this.timeout = setTimeout(() => {
    this.setState({ address });
    // }, 1000);
  };

  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        let { geometry } = results[0];
        let { location } = geometry;
        // console.log(results[0]);
        // fitBounds(geometry.bounds);

        const size = {
          width: 766, // Map width in pixels
          height: 500 // Map height in pixels
        };
        const { center, zoom } = fitBounds(
          {
            ne: {
              lat: geometry.viewport.getNorthEast().lat(),
              lng: geometry.viewport.getNorthEast().lng()
            },
            sw: {
              lat: geometry.viewport.getSouthWest().lat(),
              lng: geometry.viewport.getSouthWest().lng()
            }
          },
          size
        );
        console.log(center);
        this.setState({
          center,
          zoom,
          address: results[0].formatted_address
        });
      })
      //   .then(latLng => console.log("Success", latLng))
      .catch(error => console.error("Error", error));
  };

  dragTimeout = null;

  render() {
    return (
      <Modal
        visible={this.state.visible}
        width={800}
        close={() => this.setState({ visible: false })}>
        >
        <div className="places-modal">
          <PlacesAutocomplete
            searchOptions={{
              location: {
                lat: () => {
                  return 0.15512249999476477;
                },
                lng: () => {
                  return 37.908383000000015;
                }
              },
              radius: 600
            }}
            value={this.state.address}
            onChange={this.handleChange}
            onSelect={this.handleSelect}>
            {({
              getInputProps,
              suggestions,
              getSuggestionItemProps,
              loading
            }) => (
              <div>
                <h5>Find location</h5>

                <div className="d-flex flex-row align-items-center mt-3">
                  <MapPin
                    color="gray"
                    className="position-absolute align-self-center ml-2"
                  />
                  <input
                    {...getInputProps({
                      placeholder: "Search Places ...",
                      className: "location-search-input"
                    })}
                    className="form-control pl-5 text-black"
                  />
                </div>
                <div className="position-relative">
                  <div className="autocomplete-dropdown-container list-group position-absolute w-100">
                    {loading && (
                      <div className="list-group-item">Loading...</div>
                    )}
                    {suggestions.map(suggestion => {
                      const className = suggestion.active
                        ? "suggestion-item--active"
                        : "suggestion-item";
                      // inline style for demonstration purpose
                      const style = suggestion.active
                        ? { backgroundColor: "#fafafa", cursor: "pointer" }
                        : { backgroundColor: "#ffffff", cursor: "pointer" };
                      return (
                        <button
                          className="list-group-item list-group-item-action"
                          {...getSuggestionItemProps(suggestion, {})}>
                          <span className="text-black">
                            {suggestion.description}
                          </span>
                        </button>
                      );
                    })}
                  </div>
                </div>
              </div>
            )}
          </PlacesAutocomplete>
          <div className="map-container mt-3 position-relative">
            <img src={marker} className="map-marker" alt="" />
            <GoogleMapReact
              bootstrapURLKeys={{
                key
              }}
              defaultCenter={{ lat: -4.0434771, lng: 39.6682065 }}
              center={this.state.center}
              defaultZoom={13}
              zoom={this.state.zoom}
              bounds={this.state.bounds}
              yesIWantToUseGoogleMapApiInternals
              onDrag={map => {
                clearTimeout(this.dragTimeout);
                this.dragTimeout = setTimeout(() => {
                  // console.log(map.getCenter());
                  Geocode.fromLatLng(
                    map.getCenter().lat(),
                    map.getCenter().lng()
                  ).then(
                    response => {
                      const address = response.results[0].formatted_address;
                      this.setState({ address });
                    },
                    error => {
                      console.error(error);
                    }
                  );
                }, 300);
              }}
            />
          </div>
        </div>
        <div className="pt-3 justify-content-between d-flex flex-row align-items-center">
          <button
            className="btn btn-outline-primary px-5"
            onClick={() => this.props.set()}>
            Cancel
          </button>
          <button
            className="btn btn-primary px-5"
            onClick={() => {
              this.props.set({
                label: this.state.address,
                coords: this.state.center
              });
            }}>
            Save
          </button>
        </div>
      </Modal>
    );
  }

  componentWillReceiveProps = props => {
    if (typeof props.visible !== "undefined") {
      console.log(props.visible);
      this.setState({ visible: props.visible });
    }
  };
}

export default MapModal;
