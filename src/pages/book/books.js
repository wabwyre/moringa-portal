import React, { Component } from "react";
import Table from "../../components/Table";
import Filter from "../../components/filter";
import { Plus, UserPlus, Circle, Smartphone } from "react-feather";
import { Link } from "react-router-dom";
import Modal from "../../components/modal";
import { Verify } from "crypto";
import Fuse from "fuse.js";
import moment from "moment";
import Nav from "../../components/Nav";
import Form from "../../components/form";
import Access from "../../components/accessManager";

class Books extends Component {
  state = {
    tableData: { data: [] },
    response: { data: [] },
    tableError: false,
    query: {},
    filter: {},
    table_loading: false,
    modalVisible: false,
    addModal: false
  };
  timeout = null;
  render() {
    return (
      <div className="">
       <Nav
          name="Books"
          buttons={[
            {
              text: "Add Book",
              onClick: () => {
                this.setState({ addModal: true });
              },
              access: "all"
            }
          ]}
        ></Nav>

        <div className="mt-3 table-card p-4 border-0 card shado mx-3 shadow">
          <Table
            search={[
              "book_id"
            ]}
            sort="id"
            sortDirection={-1}
            data={this.state.tableData}
            fetch={params => {
              this.setState({ query: params });
            }}
            loading={this.state.table_loading}
            fetchError={this.state.tableError}
          />
        </div>
        
        <Modal
          visible={this.state.addModal}
          close={() => this.setState({ addModal: false })}
        >
          <div className="d-flex flex-row align-items-center">
            <UserPlus className="mr-3"></UserPlus>
            <h5 className="m-0 mt-1">Add Book</h5>
          </div>
          <div className="mt-3">
            <Form
              inputs={[
                {
                  label: "Book Name",
                  name: "name",
                  type: "text",
                  value: ""
                },
                {
                  label: "Book isbn",
                  name: "isbn",
                  type: "text",
                  value: ""
                },
                {
                  label: "Book authors",
                  name: "full_name",
                  type: "text",
                  value: ""
                },
                {
                  label: "No Pages",
                  name: "pages",
                  type: "text",
                  value: ""
                },
                {
                  label: "Book Publisher",
                  name: "publisher",
                  type: "text",
                  value: ""
                },
                {
                  label: "Country",
                  name: "country",
                  type: "text",
                  value: ""
                },
                {
                    label: "Media Type",
                    name: "media_type",
                    type: "text",
                    value: ""
                   
                },
                {
                  label: "Released",
                  name: "released",
                  type: "date",
                  value: ""
                 
              }

              ]}
              submit={data => {
                console.log(data);
                this.setState({ details: data });
                setTimeout(() => {
                  if (this.verify(data)) {
                    this.addBook(data);
                  }
                }, 0);
              }}
            />
          </div>
        </Modal>

      </div>
    );
  }

  fetchBooks = () => {
    this.setState({ table_loading: true });

    let q = {
      ...this.state.filter,
      ...this.state.query
    };

    let urlParams = Object.entries(q)
      .map(e => e.join("="))
      .join("&");
    console.log(urlParams);
    fetch(`${window.server}/books?${urlParams}`, {
      headers: {
        Authorization: localStorage.token
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        let data = [];
        response.data.map((d, i) => {
          data.push({
            book_id: d.id,
            book_name: d.name,
            isbn: d.isbn,
            authors: JSON.parse(d.authors).full_name,
            pages: d.pages,
            publisher: d.publisher,
            country: d.country,
            media_type: d.media_type,
            released: d.released,
            created_by: d.users.firstname,
            action: (
              <div className="d-flex flex-row">
                <Link
                  to={"/BookView/details/" + d.id}
                  className="btn btn-sm btn-primary px-3 btn-round"
                >
                  View
                </Link>
              </div>
            )
          });
        });
        let dts = {};
        dts.data = data;
        this.setState({
          tableData: { ...response, ...dts },
          response,
          table_loading: false
        });
      })
      .catch(d => {
        this.setState({ table_loading: false });
        console.error(d);
      });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify({ ...this.state.query, ...this.state.filter }) !==
      JSON.stringify({ ...prevState.query, ...prevState.filter })
    ) {
      let $t = this;

      clearTimeout(this.timeout);
      this.timeout = setTimeout(function() {
        $t.fetchBooks();
      }, 100);
    }
  }

  addBook = data => {
    // if (
    //   !window.confirm("Are you sure that you want add this user as portal user?")
    // )
    //   return false;
    let postData = data;
    let authors = JSON.stringify({full_name: postData.full_name})
    
   
    fetch(`${window.server}/utils/create-book`, {
      method: "POST",
      headers: {
        Authorization: localStorage.token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: postData.name,
        isbn: postData.isbn,
        pages: postData.pages,
        publisher: postData.publisher,
        country: postData.country,
        media_type: postData.media_type,
        released: postData.released,
        authors: authors
      })

    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.code) {
          alert(
            response.message +
              " \n " +
              (response.errors[0] ? response.errors[0].message : "")
          );
          this.setState({ addModal: false });
        } else {
          console.log(response);
          this.fetchBooks();

          this.setState({ addModal: false });
        }
      })
      .catch(d => {
        console.log("Error saving the data");
        console.log(d);
        this.setState({ modalVisible: false });
      });
  };

  verify(data) {
    let result = true;
    let missing = [];
    Object.keys(data).map(d => {
      if (!data[d] || data[d] === "") {
        missing.push(d.replace("_id", "").replace(/_/g, " "));
        result = false;
      }
    });
    missing.join(", ");
    if (!result) alert("Please fill all the require fields : " + missing);
    return result;
  }
}

export default Books;
