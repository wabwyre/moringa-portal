import React, { Component } from "react";
import moment from "moment";
import start from "../img/logo.png";
import Nav from "../components/Nav";
import stamp from "../img/logo.png";
import {
  Calendar,
  DollarSign,
  Users,
  MessageSquare,
  BookOpen,
  Target,
  TrendingDown
} from "react-feather";
import logo from "../img/logo.png";
import * as utils from "../utils/utils";

class Home extends Component {
  state = {

    summarries: {},
    graphData: [],
    updateCount: false,
    showGraph: true,
    checked: []
  };
  render() {
    return (
      <div>
        <div className="px-3 my-5">
          <div className="d-flex flex-row align-items-center">
            <img src={logo} className="dasboard-logo mr-3" alt="" />
            <div>
              <h1 className="mb-0 font-weight-bold text-primary">
                Moringa
              </h1>
            </div>
          </div>
        </div>

        <div className="page-content px-3 position-relative">
          <div className="card py-4 px-5 mt-2 intro-card my-5">
            <div className="row">
              <div className="col-md-12">
                <h4 className="font-weight-bold intro-title">Dashboard</h4>
                <div className="mt-3">
                  To get started, use the menu on your left. All the general
                  summaries will be displayed on this page. Any new system
                  updates will be displayed here.
                </div>
              </div>
              {/* <div className="col-md-4 flex-row justify-content-center d-md-flex d-none">
                <img src={start} alt="" className="intro-img" />
              </div> */}
            </div>
          </div>

          <div className="px-3">
          <div className="row mb-5">
            <div className='col-md-3 intro tab'>
              <div className='card rounded p-3 d-flex flex-row align-items-center text-white material-blue'>
                <div className='icon-holder p-3 mr-3'>
                  <Users/>
                </div>
                <div>
                <div className="text-capitalize">Users</div>
                <div className='mt-1 font-weight-bold'>
                      <small>
                        <small> </small>
                      </small>{' '}
                </div>
                </div>
              </div>
            </div>
            <div className='col-md-3 intro tab'>
              <div className='card rounded p-3 d-flex flex-row align-items-center text-white material-green'>
                <div className='icon-holder p-3 mr-3'>
                  <BookOpen/>
                </div>
                <div>
                <div className="text-capitalize">Books</div>
                <div className='mt-1 font-weight-bold'>
                      <small>
                        <small></small>
                      </small>{' '}
                </div>
                </div>
              </div>
            </div>
            <div className='col-md-3 intro tab'>
              <div className='card rounded p-3 d-flex flex-row align-items-center text-white material-red'>
                <div className='icon-holder p-3 mr-3'>
                  <Users/>
                </div>
                <div>
                <div className="text-capitalize">Characters</div>
                <div className='mt-1 font-weight-bold'>
                      <small>
                        <small></small>
                      </small>{' '}
                </div>
                </div>
              </div>
            </div>
            <div className='col-md-3 intro tab'>
              <div className='card rounded p-3 d-flex flex-row align-items-center text-white material-deep-purple'>
                <div className='icon-holder p-3 mr-3'>
                  <MessageSquare/>
                </div>
                <div>
                <div className="text-capitalize">Comments</div>
                <div className='mt-1 font-weight-bold'>
                      <small className='opacity-0'>
                        <small> </small>
                      </small>{' '}
                </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    );
  }

  
  componentDidMount = () => {
    
  };

}

export default Home;

