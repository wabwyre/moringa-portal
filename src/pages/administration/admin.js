import React, { Component } from "react";
import Table from "../../components/Table";
import Filter from "../../components/filter";
import { Plus, UserPlus, Circle, Smartphone } from "react-feather";
import { Link } from "react-router-dom";
import Modal from "../../components/modal";
import { Verify } from "crypto";
import Fuse from "fuse.js";
import moment from "moment";
import Nav from "../../components/Nav";
import Form from "../../components/form";
import Access from "../../components/accessManager";

class Admins extends Component {
  state = {
    tableData: { data: [] },
    response: { data: [] },
    tableError: false,
    query: {},
    filter: {},
    table_loading: false,
    modalVisible: false,
    addModal: false
  };
  timeout = null;
  render() {
    return (
      <div className="">
       <Nav
          name="Portal Users"
          buttons={[
            {
              text: "Add Portal User",
              onClick: () => {
                this.setState({ addModal: true });
              },
              access: "all"
            }
          ]}
        ></Nav>

        <div className="mt-3 table-card p-4 border-0 card shado mx-3 shadow">
          <Table
            search={[
              "firstname", "lastname"
            ]}
            sort="id"
            sortDirection={-1}
            data={this.state.tableData}
            fetch={params => {
              this.setState({ query: params });
            }}
            loading={this.state.table_loading}
            fetchError={this.state.tableError}
          />
        </div>
        
        <Modal
          visible={this.state.addModal}
          close={() => this.setState({ addModal: false })}
        >
          <div className="d-flex flex-row align-items-center">
            <UserPlus className="mr-3"></UserPlus>
            <h5 className="m-0 mt-1">Add Portal User</h5>
          </div>
          <div className="mt-3">
            <Form
              inputs={[
                {
                  label: "First name",
                  name: "firstname",
                  type: "text",
                  value: ""
                },
                {
                    label: "Last name",
                    name: "lastname",
                    type: "text",
                    value: ""
                  },
                {
                  label: "Email",
                  name: "email",
                  type: "email",
                  value: ""
                },
                // {
                //   label: "Access Role",
                //   name: "group_id",
                //   value: "",
                //   type: "select",
                //   options: window.groups.reverse().map(d => {
                //     return { name: d.name, value: d.id };
                //   })
                // }
              ]}
              submit={data => {
                console.log(data);
                this.setState({ details: data });
                setTimeout(() => {
                  if (this.verify(data)) {
                    this.addUser(data);
                  }
                }, 0);
              }}
            />
          </div>
        </Modal>

      </div>
    );
  }

  fetchAdmins = () => {
    this.setState({ table_loading: true });

    let q = {
      ...this.state.filter,
      ...this.state.query
    };

    let urlParams = Object.entries(q)
      .map(e => e.join("="))
      .join("&");
    console.log(urlParams);
    fetch(`${window.server}/users?${urlParams}`, {
      headers: {
        Authorization: localStorage.token
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        let data = [];
        response.data.map((d, i) => {
          data.push({
            id: d.id,
            user_id: d.user_id,
            full_name: `${d.firstname} ${d.lastname}`,
            email: d.email,
            role: d.roles.name,
            action: (
              <div className="d-flex flex-row">
                <Link
                  to={"/PortalView/details/" + d.id}
                  className="btn btn-sm btn-primary px-3 btn-round"
                >
                  View
                </Link>
              </div>
            )
          });
        });
        let dts = {};
        dts.data = data;
        this.setState({
          tableData: { ...response, ...dts },
          response,
          table_loading: false
        });
      })
      .catch(d => {
        this.setState({ table_loading: false });
        console.error(d);
      });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify({ ...this.state.query, ...this.state.filter }) !==
      JSON.stringify({ ...prevState.query, ...prevState.filter })
    ) {
      let $t = this;

      clearTimeout(this.timeout);
      this.timeout = setTimeout(function() {
        $t.fetchAdmins();
      }, 100);
    }
  }

  addUser = data => {
    // if (
    //   !window.confirm("Are you sure that you want add this user as portal user?")
    // )
    //   return false;
    let postData = data;
   
    fetch(`${window.server}/users/registration`, {
      method: "POST",
      headers: {
        Authorization: localStorage.token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        firstname: postData.firstname,
        lastname: postData.lastname,
        email: postData.email
        // group_id : postData.group_id
      })
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.code) {
          alert(
            response.message +
              " \n " +
              (response.errors[0] ? response.errors[0].message : "")
          );
          this.setState({ addModal: false });
        } else {
          console.log(response);
          this.fetchAdmins();

          this.setState({ addModal: false });
        }
      })
      .catch(d => {
        console.log("Error saving the data");
        console.log(d);
        this.setState({ modalVisible: false });
      });
  };

  verify(data) {
    let result = true;
    let missing = [];
    Object.keys(data).map(d => {
      if (!data[d] || data[d] === "") {
        missing.push(d.replace("_id", "").replace(/_/g, " "));
        result = false;
      }
    });
    missing.join(", ");
    if (!result) alert("Please fill all the require fields : " + missing);
    return result;
  }
}

export default Admins;
