import React, { Component } from "react";
import { Route, Link, Switch } from "react-router-dom";
import {
  Plus,
  Edit,
  Check,
  UserPlus,
  AlertTriangle,
  ThumbsUp,
  RefreshCw,
  Edit2,
  UserX, XCircle, CheckCircle
} from "react-feather";
import Tabs from "../../components/tabs";
import Modal from "../../components/modal";
import Form from "../../components/form";
import Access from "../../components/accessManager";
import LButton from "../../components/loadingButton";
import Details from "./details";
import config from "../../config";

class CharacterView extends Component {
  state = {
    currentRoute: "",
    editModal: false,
    loadingStatus: 0
  };
  render() {
    let user = [];

    return (
      <div className="bg-light">
        <div className="card table-card m-3">
          <div className="text-mute pt-3 pl-3">
            <small className="text-mute">Characters View</small>
          </div>

          <div className="profile p-3 d-md-flex flex-row align-items-center justify-content-between">
            <div className="d-md-flex flex-row align-items-center">
              <div className="border avatar-lg bg-light d-flex flex-row align-items-center justify-content-center">
                <span className="initials">
                  {this.state.name ? this.state.name[0] : ""}
                </span>
                <img
                  src={`${window.server}${this.state.passport_path &&
                    this.state.passport_path.replace("public", "")}`}
                  className="avatar"
                  alt=""
                />
              </div>
              <div className="ml-md-4 my-3 my-md-0">
                <h4 className="text-capitalize">
                  {this.state.name &&
                    (
                      this.state.name 
                    ).toLowerCase()}
                </h4>
                <div className="ml-2 mt-1">
                  <span className="badge badge-secondary px-1">Character</span>
                </div>
              </div>
            </div>

            <div className="d-md-flex flex-row align-items-center justify-content-center text-center">
               <div className=" ml-3">
                {this.state.id && (
                  <Access permission="all">
                    <button
                      onClick={() => {
                        this.setState({ editModal: true });
                        //console.log(this.state.response.data[0]);
                      }}
                      className="option-card no-wrap pr-3 d-md-flex d-inline-block my-2 flex-row btn align-items-center btn-primary btn-round mr-3"
                    >
                      <Edit size={18} />
                      <span className="pl-1 font-weight-bold">Edit Character</span>
                    </button>
                  </Access>
                )}
              </div>

            </div>
            {/* </Access> */}
          </div>

          <Tabs
            tabs={[
              {
                label: "DETAILS",
                link:
                  "/CharacterView/details/" +
                  this.props.match.params.id,
                access: "all"
              }
            ]}
          ></Tabs>
        </div>
        <Route
          path="/CharacterView/details/:id"
          exact
          component={Details}
        />

       <Modal
          visible={this.state.editModal}
          close={() => this.setState({ editModal: false })}
        >
          <div className="d-flex flex-row align-items-center">
            <UserPlus className="mr-3"></UserPlus>
            <h5 className="m-0 mt-1">Edit Character</h5>
          </div>

          {this.state.id && (
          <div className="mt-3">
            <Form
              inputs={[
                {
                  label: "Character name",
                  name: "name",
                  type: "text",
                  value: this.state.name
                },
                {
                  label: "Gender",
                  name: "gender",
                  value: this.state.lastname,
                  type: "select",
                  options: [
                    { name: "male", value: "male" },
                    { name: "female", value: "female" }
                  ]
                },
                {
                  label: "Culture",
                  name: "culture",
                  type: "text",
                  value: this.state.culture
                },
                {
                  label: "Age",
                  name: "age",
                  type: "text",
                  value: this.state.age
                },
                {
                  label: "Actor names",
                  name: "full_name",
                  type: "text",
                  value: this.state.played_by.full_name
                }
              ]}
              submit={data => {
                console.log(data);
                this.setState({ details: data });
                setTimeout(() => {
                  if (this.verify(data)) {
                    this.editCharacter(data);
                  }
                }, 0);
              }}
            />
          </div>
           )}{" "}
        </Modal>

      </div>
    );
  }

  componentDidMount = () => {
    this.fetch();
  };

  fetch = () => {
    this.setState({ table_loading: true });

    fetch(`${window.server}/characters?id=${this.props.match.params.id}`, {
      headers: {
        Authorization: localStorage.token
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        
        this.setState({
          ...response.data[0],
          table_loading: false
        });
      })
      .catch(d => {
        this.setState({ table_loading: false });
        console.error(d);
      });
  };

  verify(data) {
    let result = true;
    let missing = [];
    Object.keys(data).map(d => {
      if (!data[d] || data[d] === "") {
        missing.push(d.replace("_id", "").replace(/_/g, " "));
        result = false;
      }
    });
    missing.join(", ");
    if (!result) alert("Please fill all the require fields : " + missing);
    return result;
  }


  editCharacter = data => {
    // if (
    //   !window.confirm("Are you sure that you want add this user as portal user?")
    // )
    //   return false;
    let user_id = this.props.match.params.id;
    let postData = data;
    let played_by = JSON.stringify({full_name: postData.full_name});
   
    fetch(`${window.server}/characters/${user_id}`, {
      method: "PATCH",
      headers: {
        Authorization: localStorage.token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: postData.name,
        gender: postData.gender,
        culture: postData.culture,
        age: postData.age,
        played_by: played_by
      })
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.code) {
          alert(
            response.message +
              " \n " +
              (response.errors[0] ? response.errors[0].message : "")
          );
          this.setState({ addModal: false });
        } else {
          console.log(response);
          this.fetchCharacters();

          this.setState({ addModal: false });
        }
      })
      .catch(d => {
        console.log("Error saving the data");
        console.log(d);
        this.setState({ modalVisible: false });
      });
  };

}

export default CharacterView;
