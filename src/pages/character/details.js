import React, { Component } from 'react';

class Details extends Component {
  state = { data: [{}] };
  render() {
    return (
      <div className='p-3'>
        <ul className='list-group user-details'>
          {Object.keys(this.state.data[0]).map(d => (
            <li className='list-group-item d-flex flex-row list-group-item-action text-dark'>
              <div className='user-detail-title font-weight-bold text-capitalize'>
                {d}
              </div>
              <div>{this.state.data[0][d]}</div>
            </li>
          ))}
        </ul>
      </div>
    );
  }

  componentDidMount = () => {
    this.fetch();
  };

  fetch = () => {
    fetch(`${window.server}/characters?id=${this.props.match.params.id}`, {
      headers: {
        Authorization: localStorage.token
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        let data = [];
        response.data.map((d, i) => {
          data.push({
            id:d.id,
            'Character Name': (
              <span className='text-capitalize'>
                {d.name}
              </span>
            ),
            gender: d.gender,
            culture: d.culture,
            age: d.age,
            played_by: JSON.parse(d.played_by).full_name,
            'Created By': (
                <span className='text-capitalize'>
                  {d.users.firstname}
                </span>
              ),
           
          });
        });

        this.setState({
          data
        });
      })
      .catch(d => {
        this.setState({ table_loading: false });
        console.error(d);
      });
  };
}

export default Details;
